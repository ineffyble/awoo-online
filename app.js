//environment vars
var envVars = require('./vars.js');

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Routing
var mongooseAwoo = require('./awooSchema.js');
var index = require('./routes/index');
var api = require('./routes/api')
//var postAwoo = require('./routes/postAwoo');

app.use('/', index);
app.use('/api', api)
//app.use('/awoo', postAwoo);

module.exports = app;