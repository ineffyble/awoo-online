# Awoo Online

Features:

Gradient text! 
Awoo at your friends!
No user data at the moment! 

### Contributing

Contributions are welcome! Documentation, examples, code, and feedback - they all help.

Be sure to review the included code of conduct. This project adheres to the Contributor's Covenant (http://contributor-covenant.org/). By participating in this project you agree to abide by its terms.


### To run:
Clone the repo to a directory of your choice.

Install node (from here: https://nodejs.org/en/, the latest version is fine).

Then run (in the repo directory)
```
$npm install
```
Make a file called 'vars.js' like this in the base dir:
```javascript
module.exports = {
	mongooseUser: "username",
	mongoosePass: "password",
    mongooseHostName: "web.site/database"
};
```
and then use
```
$npm start
```
and the site should be at localhost:3000

The server will connect to the mongoDB at:
```
mongodb://username:password@web.site/database
```
read awooSchema.js for more database-y stuff. currently it's mostly dummied out for laziness elsewhere, but awooSchema and userSchema

### To do:

User account-y things with twitter auth
form posting awoots
prettyness
