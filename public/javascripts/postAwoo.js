function postAwoo(awoo) {
    fetch('/api/awoots', {
        method: 'post',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(awoo)
    })
    .then(function(res) {
        return res.json();
    })
    .then(function(data) {
        console.log(data);
    }).catch(function(error) {
        console.log(error);
    });
};