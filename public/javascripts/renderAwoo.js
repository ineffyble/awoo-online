//Converts an awoo to the specified case - lowerCase, upperCase, titleCase, inverseTitleCase, wiggleCase, smallCaps, upsideDown - and then decorates it with the prefix and suffix and decoration emoji. 
module.exports.renderAwoo = function (awoo) {
    var awooText = awoo.content;
    switch (awoo.case) {
        case 'lowerCase':
            awooText = awooText.toLowerCase();
            break;
        case 'upperCase': 
            awooText = awooText.toUpperCase();
            break;
        case 'titleCase':
            awooText = awooText.toLowerCase();
            firstLetter = awooText.substr(0,1).toUpperCase();
            awooText = firstLetter + awooText.substr(1);
            break;
        case 'inverseTitleCase':
            awooText = awooText.toUpperCase();
            firstLetter = awooText.substr(0,1).toLowerCase;
            awooText = firstLetter + awooText.substr(1);
            break;
        case 'wiggleCase': 
            length = awooText.length;
            newText = "";
            for ( i = 0; i < length; i++) {
                if (i % 2 == 0) {
                    newText = newText + awooText.charAt(i).toLowerCase();
                } else {
                    newText = newText + awooText.charAt(i).toUpperCase();
                }
            }
            awooText = newText;
            break;
        case 'smallCaps':
            var smallCaps = 'ᴀʙᴄᴅᴇꜰɢʜɪᴊᴋʟᴍɴᴏᴘǫʀsᴛᴜᴠᴡxʏᴢ';
            isAwoo = (awooText.toLowerCase().substr(0,2) == 'aw');
            if (isAwoo) {
                awooLength = awooText.length;
                awooText = 'ᴀᴡ'+'ᴏ'.repeat(awooLength-2);
            }
            break;
        case 'upsideDown': 
            var upsideDown = 'ɐqɔpǝⅎƃɥᴉɾʞʅɯuoddɹsʇnʌʍxʎz';
            isAwoo = (awooText.toLowerCase().substr(0,2) == 'aw');
            if (isAwoo) {
                awooLength = awooText.length;
                awooText = 'o'.repeat(awooLength-2) + 'ʍɐ';
            }
            break; 
    }
    var prefix = awoo.affix.join('');
    var suffix = awoo.affix.reverse().join('');
    var decoration = awoo.decoration;
    var awoot = prefix + awooText + suffix + decoration;
    return awoot;
}
